import disposableMockDate from "../src";
import { match, P } from "ts-pattern";

// While typescript is happy to convert the "using" keyword into something
// functional, but does not want to do any actual polyfill-ing.

// In general, it's expected that you might use something like core-js or
// babel to provide polyfills. In this case, however, that's over-kill:
// all we need here is Symbol.dispose, and we can get it by just doing this:
// @ts-expect-error
Symbol.dispose ??= Symbol("Symbol.dispose");
// See https://devblogs.microsoft.com/typescript/announcing-typescript-5-2-beta/

describe("disposableMockDate", () => {
  it.each(["2020-02-02", new Date("2020-02-02"), 1000])(
    "sets the date with %s",
    (value) => {
      const valueDate = match(value)
        .with(P.string, (v) => new Date(v))
        .with(P.instanceOf(Date), (v) => v)
        .with(P.number, (v) => new Date(v))
        .exhaustive();

      using _ = disposableMockDate(value);
      expect(Date.now()).toStrictEqual(valueDate.valueOf());
      expect(new Date()).toStrictEqual(valueDate);
    }
  );

  it("un-sets the date out of the scope", () => {
    (() => {
      using _ = disposableMockDate(new Date(2020, 1, 2));
    })();
    expect(Date.now()).toBeGreaterThan(new Date(2020, 1, 2).valueOf());
  });
});
