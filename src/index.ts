import { set, reset } from "mockdate";
/**
 * Set and automatically revert the current date
 *
 * This is intended to be used with the `using` keyword. For example,
 * you can use it to prevent bleeding in a Jest test:
 *
 * ```ts
 * it("is always good to have predicable inputs", () => {
 *   using _ = disposableMockDate('2020-02-02');
 *   expect(new Date()).toStrictEqual(new Date('2020-02-02'))
 * });

 * it("will not work here because it cleans itself up automatically", () => {
 *   expect(new Date()).toStrictEqual(new Date('2020-02-02'))
 * });
 * ```
 *
 * @param overrides key-value object of environment variable names -> values
 */
export default function disposableMockDate(
  date: string | number | Date
): Disposable {
  set(date);
  return {
    [Symbol.dispose]() {
      reset();
    },
  };
}
